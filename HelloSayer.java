package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws Exception {
        // Binary hello sayer
        // while picked 1 it says hello world
        // while picked 0 it says bye world
        // while picked other number it returns an error

        BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter your num here - ");
        boolean isTrue = true;
        while (isTrue) {
            int scan = Integer.parseInt(read.readLine());
            String var = scan == 1 ? "Hello" : "Bye";
            String str = scan == 1 || scan == 0 ? ( var + ", World") : "Retry: ";
            isTrue = scan != 1 && scan != 0;
            System.out.print(str);
        }

    }
}
